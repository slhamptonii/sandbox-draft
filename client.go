package main

import (
	"encoding/json"
	"fmt"
	"log"
	"sync"

	"github.com/google/uuid"
	"github.com/gorilla/websocket"
)

type Client struct {
	Id     string
	Conn   *websocket.Conn
	Pool   *Pool
	RoomId string
	mu     sync.Mutex
}

type Message struct {
	ClientId string `json:"clientId"`
	RoomId   string `json:"roomId"`
	Type     int    `json:"type"`
	Command  string `json:"command"`
	Error    string `json:"error"`
	Body     string `json:"body"`
}

func (c *Client) sendMessage(message Message) {
	if err := c.Conn.WriteJSON(message); err != nil {
		log.Println("error writing to client", err.Error())
	}
}

func (c *Client) Read() {
	defer func() {
		if c.Pool != nil {
			c.Pool.Unregister <- c
		}
		c.Conn.Close()
	}()

	for {
		_, p, err := c.Conn.ReadMessage()
		if err != nil {
			log.Println("error reading message", err)
			return
		}

		var message Message
		err = json.Unmarshal(p, &message)
		if err != nil {
			log.Println("error reading message", err.Error())
			continue
		}

		fmt.Printf("Message Received: %+v\n", message)

		switch message.Command {
		case "create-room":
			pool := NewPool()
			go pool.Start()
			poolId := uuid.New().String()
			roomPoolMap[poolId] = pool
			c.sendMessage(Message{RoomId: poolId})
			break
		case "join-room":
			if message.RoomId == "" || message.ClientId == "" {
				c.sendMessage(Message{Error: "invalid command"})
				break
			}

			pool := roomPoolMap[message.RoomId]
			if pool == nil {
				c.sendMessage(Message{Error: "not found"})
				break
			}

			client := clientMap[message.ClientId]
			if client == nil {
				c.sendMessage(Message{Error: "unauthorized"})
				break
			}

			client.Pool = pool
			pool.Register <- client

			c.sendMessage(Message{Body: "ok"})
			break
		case "broadcast-message":
			if c.Pool != nil && message.RoomId == c.Pool.Id {
				c.Pool.Broadcast <- message
			}
		}

	}
}
