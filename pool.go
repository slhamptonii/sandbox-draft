package main

import "fmt"

type Pool struct {
	Id         string
	Register   chan *Client
	Unregister chan *Client
	Clients    map[*Client]bool
	Broadcast  chan Message
}

func NewPool() *Pool {
	return &Pool{
		Register:   make(chan *Client),
		Unregister: make(chan *Client),
		Clients:    make(map[*Client]bool),
		Broadcast:  make(chan Message),
	}
}

func (pool *Pool) Start() {
	for {
		select {
		case client := <-pool.Register:
			pool.Clients[client] = true
			fmt.Println("Size of Connection Pool: ", len(pool.Clients))
			fmt.Println("registered client", client)
			for c, _ := range pool.Clients {
				if c.Id == client.Id {
					continue
				}
				c.sendMessage(Message{RoomId: pool.Id, Type: 1, Body: fmt.Sprintf("%s joined the room", client.Id)})
			}
			break
		case client := <-pool.Unregister:
			delete(pool.Clients, client)
			fmt.Println("Size of Connection Pool: ", len(pool.Clients))
			for c, _ := range pool.Clients {
				if c.Id == client.Id {
					continue
				}
				c.sendMessage(Message{RoomId: pool.Id, Type: 1, Body: fmt.Sprintf("%s left the room", client.Id)})
			}
			break
		case message := <-pool.Broadcast:
			fmt.Println("Sending message to all clients in Pool")
			for client, _ := range pool.Clients {
				if client.Id == message.ClientId {
					continue
				}
				client.sendMessage(message)
			}
		}
	}
}
