package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/google/uuid"
	"github.com/gorilla/websocket"
)

var roomPoolMap map[string]*Pool
var clientMap map[string]*Client

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func main() {
	roomPoolMap = make(map[string]*Pool, 0)
	clientMap = make(map[string]*Client, 0)

	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		upgrader.CheckOrigin = func(r *http.Request) bool { return true } // allow all connections w/out CORS error

		conn, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			log.Println("error upgrading connection", err.Error())
			fmt.Fprintf(w, "%+v\n", err)
		}

		log.Println("Client connected successfully")

		client := &Client{
			Id:   uuid.New().String(),
			Conn: conn,
		}

		fmt.Println("client to be registered", client.Id)
		clientMap[client.Id] = client

		go client.Read()

		client.sendMessage(Message{ClientId: client.Id})
	})

	log.Fatal(http.ListenAndServe(":8080", nil))
}
